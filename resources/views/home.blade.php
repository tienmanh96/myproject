<!DOCTYPE html>
	<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Home Admin</title>
			<!-- Latest compiled and minified CSS & JS -->
			<meta name="csrf-token" content="{{ csrf_token() }}">
	<script src="//code.jquery.com/jquery.js"></script>
	
	 <meta name="csrf-token" content="{{ csrf_token() }}">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
			<script src="//code.jquery.com/jquery.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
			<link rel="stylesheet" type="text/css" href="{{asset('/css/style.css')}}">
			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

		</head>
		<body>
			<div class="wrapper">
					<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<!-- <img src="{{asset('/img/anh.jpg')}}"style="width: 50px;height: 50px;"> -->
					<form class="navbar-form  navbar-right" role="search">
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Search">
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
					</form>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav">
							<li><a class="navbar-brand fa fa-home" href="{{route('products.index')}}">Trang Chủ </a></li>
							<li><a href="{{route('products.index')}}">Sản Phẩm </a></li>
							<li><a href="{{route('categories.index')}}">Loại Sản Phẩm </a></li>
							<li><a href="#">Liên Hệ </a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<p></p> {{Auth::user()->name}}<b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="{{url('logout')}}">Đăng xuất </a></li>
									<li><a href="#">Đổi mật khẩu </a></li>
								</ul>
							</li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
					<table class="table table-hover">
						<thead>

							<h2 style="text-align: center;">Danh Sách Sản Phẩm </h2>
							<tr>
								<button type="button" name="create_product" id="create_product" class="btn btn-success"  data-toggle="modal" data-target="#formModal">Thêm </button>
								<th>STT</th>
								<th>TYPE PRODUCTS</th>
								<th>NAME PRODUCTS</th>
								<th>IMAGE</th>
								<th>PRICE</th>
								<th>ACTION</th>
							</tr>
						</thead>
						<tbody>
							@foreach($productss as $products)
							<tr>
								<td>{{$products->id}}</td>
								<td>{{$products->category_id}}</td>
								<td>{{$products->name}}</td>
								<td>{{$products->image}}</td>
								<td>{{$products->price}}.vnd</td>
								<td>
									<a id="{{ $products->id }}" class="btn btn-danger del"> Delete</a>
									<a id="" class="btn btn-danger "> EDIT</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>	
					<div aria-label="Page navigation right" style="float: right;">
						{{$productss->links()}}
					</div>
				</nav>
				<p style="float: right;">&copy; Nguyen Tien Manh<p>
			</div>
	@include('product.action_product')
		</body>
		<script>
      $.ajaxSetup({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
	$('.del').on('click', function () {
								 //confirm("Are You sure want to delete !");
	 var products_id = $(this).attr("id");
	 console.log(products_id);
	 $.ajax({
	type: "DELETE",
	 url: "{{ URL::to('/') }}/products/"+products_id,

	 success: function (data) {
	console.log('success:', data);
	 window.location="http://localhost/products";
	   },
	 error: function (data) {
	 console.log('Error:', data);
	 }
	});
 });
	$('#create_product')on('click',function(){
		 $('#formModal').modal('show');
	});

	</script>
</html>